# Gentoo AMD64 Handbook

This is a LaTeX version of the [Gentoo AMD64 Handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64). The handbook content is licensed under the [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) license. 

Graphics used in the file:
* The Gentoo signet. Gentoo Foundation and Lennart Andre Rolland/[CC-BY-SA 2.5](https://creativecommons.org/licenses/by-sa/2.5/)
* Larry. By Ethan Dunham/[CC-BY-SA 2.5](https://creativecommons.org/licenses/by-sa/2.5/)

I'm not yet sure which (free) license makes sense for the LaTeX files. Their license will follow when I made up my mind.
